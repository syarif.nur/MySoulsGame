package com.threedotz.mysoulsgame

import android.app.Application
import com.threedotz.mysoulsgame.core.di.databaseModule
import com.threedotz.mysoulsgame.core.di.networkModule
import com.threedotz.mysoulsgame.core.di.repositoryModule
import com.threedotz.mysoulsgame.di.useCaseModule
import com.threedotz.mysoulsgame.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class MyApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger(Level.NONE)
            androidContext(this@MyApplication)
            modules(listOf(
                databaseModule,
                networkModule,
                repositoryModule,
                useCaseModule,
                viewModelModule,
            ))
        }
    }
}