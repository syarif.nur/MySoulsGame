package com.threedotz.mysoulsgame.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.threedotz.mysoulsgame.core.domain.usecase.GameUseCase

class HomeViewModel(gameUseCase: GameUseCase) : ViewModel() {
    val game = gameUseCase.getAllGame().asLiveData()
}