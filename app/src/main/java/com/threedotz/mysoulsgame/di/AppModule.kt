package com.threedotz.mysoulsgame.di

import com.threedotz.mysoulsgame.core.domain.usecase.GameInteractor
import com.threedotz.mysoulsgame.core.domain.usecase.GameUseCase
import com.threedotz.mysoulsgame.detail.DetailGameViewModel
import com.threedotz.mysoulsgame.home.HomeViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val useCaseModule = module {
    factory<GameUseCase> { GameInteractor(get()) }
}

val viewModelModule = module {
    viewModel { HomeViewModel(get()) }
    viewModel { DetailGameViewModel(get()) }

}