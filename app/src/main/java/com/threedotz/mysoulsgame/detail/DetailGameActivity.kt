package com.threedotz.mysoulsgame.detail

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.threedotz.mysoulsgame.R
import com.threedotz.mysoulsgame.core.domain.model.Game
import com.threedotz.mysoulsgame.databinding.ActivityDetailGameBinding
import org.koin.android.viewmodel.ext.android.viewModel

class DetailGameActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetailGameBinding
    private val detailGameViewModel: DetailGameViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailGameBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)
        val detailGame = intent.getParcelableExtra<Game>(EXTRA_DATA)
        showDetailGame(detailGame)
    }

    private fun showDetailGame(detailGame: Game?) {
        detailGame?.let {
            supportActionBar?.title = detailGame.name
            binding.content.tvDetailRelease.text = detailGame.released
            binding.content.tvDetailRating.text = detailGame.rating
            binding.content.tvDetailMetacritic.text = detailGame.metacritic
            Glide.with(this@DetailGameActivity)
                .load(detailGame.imageUrl)
                .into(binding.ivDetailImage)

            var statusFavorite = detailGame.isFavorite
            setStatusFavorite(statusFavorite)
            binding.fab.setOnClickListener {
                statusFavorite = !statusFavorite
                detailGameViewModel.setFavoriteGame(detailGame, statusFavorite)
                setStatusFavorite(statusFavorite)
                if (statusFavorite) {
                    Toast.makeText(this, "Save to Favorite", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this, "Remove from Favorite", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun setStatusFavorite(statusFavorite: Boolean) {
        if (statusFavorite) {
            binding.fab.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.baseline_favorite_24))
        } else {
            binding.fab.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.baseline_favorite_border_24))
        }
    }

    companion object {
        const val EXTRA_DATA = "extra_data"
    }
}