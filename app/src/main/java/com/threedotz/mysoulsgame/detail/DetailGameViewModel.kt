package com.threedotz.mysoulsgame.detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.threedotz.mysoulsgame.core.domain.model.Game
import com.threedotz.mysoulsgame.core.domain.usecase.GameUseCase
import kotlinx.coroutines.launch

class DetailGameViewModel(private val useCase: GameUseCase) : ViewModel() {
    fun setFavoriteGame(game: Game, status: Boolean) {
        viewModelScope.launch {
            useCase.setFavoriteGame(game, status)
        }
    }
}