package com.threedotz.mysoulsgame.core.data.source.local

import com.threedotz.mysoulsgame.core.data.source.local.entitiy.GameEntity
import com.threedotz.mysoulsgame.core.data.source.local.room.GameDao
import kotlinx.coroutines.flow.Flow

class LocalDataSource(private val gameDao: GameDao) {

    fun getAllGame(): Flow<List<GameEntity>> = gameDao.getAllGame()

    fun getFavoriteGame():Flow<List<GameEntity>> = gameDao.getFavoriteGame()

    suspend fun insertGame(gameList: List<GameEntity>) = gameDao.insertGame(gameList)

    suspend fun setFavoriteGame(game: GameEntity, newState: Boolean){
        game.isFavorite = newState
        gameDao.updateFavoriteGame(game)

    }
}