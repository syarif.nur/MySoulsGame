package com.threedotz.mysoulsgame.core.domain.usecase


import com.threedotz.mysoulsgame.core.data.Resource
import com.threedotz.mysoulsgame.core.domain.model.Game
import com.threedotz.mysoulsgame.core.domain.repository.IGameRepository
import kotlinx.coroutines.flow.Flow

class GameInteractor(private val gameRepository: IGameRepository) : GameUseCase {
    override fun getAllGame(): Flow<Resource<List<Game>>> = gameRepository.getAllGame()

    override fun getFavoriteGame(): Flow<List<Game>> = gameRepository.getFavoriteGame()

    override suspend fun setFavoriteGame(game: Game, state: Boolean) = gameRepository.setFavoriteGame(game, state)
}