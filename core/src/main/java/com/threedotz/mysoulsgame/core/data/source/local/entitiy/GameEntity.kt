package com.threedotz.mysoulsgame.core.data.source.local.entitiy


import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "game_list")
data class GameEntity (

    @PrimaryKey
    @ColumnInfo(name = "gameId")
    var id: String,

    @ColumnInfo(name = "name")
    var name: String,

    @ColumnInfo(name = "released")
    var released: String,

    @ColumnInfo(name = "imageUrl")
    var imageUrl: String?,

    @ColumnInfo(name = "rating")
    var rating: String?,

    @ColumnInfo(name = "metacritic")
    var metacritic: String?,

    @ColumnInfo(name = "isFavorite")
    var isFavorite: Boolean

)