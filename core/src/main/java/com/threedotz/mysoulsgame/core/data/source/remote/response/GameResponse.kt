package com.threedotz.mysoulsgame.core.data.source.remote.response

import com.google.gson.annotations.SerializedName

data class GameResponse(

    @field:SerializedName("id")
    val id: String,

    @field:SerializedName("name")
    val name: String,

    @field:SerializedName("released")
    val released: String,

    @field:SerializedName("background_image")
    val imageUrl: String,

    @field:SerializedName("rating")
    val rating: String,

    @field:SerializedName("metacritic")
    val metacritic: String,


)