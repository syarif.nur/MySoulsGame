package com.threedotz.mysoulsgame.core.data

import com.threedotz.mysoulsgame.core.data.source.local.LocalDataSource
import com.threedotz.mysoulsgame.core.data.source.remote.RemoteDataSource
import com.threedotz.mysoulsgame.core.data.source.remote.network.ApiResponse
import com.threedotz.mysoulsgame.core.data.source.remote.response.GameResponse
import com.threedotz.mysoulsgame.core.domain.model.Game
import com.threedotz.mysoulsgame.core.domain.repository.IGameRepository
import com.threedotz.mysoulsgame.core.utils.DataMapper
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class GameRepository(
    private val remoteDataSource: RemoteDataSource,
    private val localDataSource: LocalDataSource,
) : IGameRepository {

    override fun getAllGame(): Flow<Resource<List<Game>>> {
        return object : NetworkBoundResource<List<Game>, List<GameResponse>>() {
            override fun loadFromDB(): Flow<List<Game>> {
                return localDataSource.getAllGame().map {
                    DataMapper.mapEntitiesToDomain(it)
                }
            }

            override suspend fun createCall(): Flow<ApiResponse<List<GameResponse>>> =
                remoteDataSource.getAllGame()

            override suspend fun saveCallResult(data: List<GameResponse>) {
                val gameList = DataMapper.mapResponseToEntities(data)
                localDataSource.insertGame(gameList)
            }

            override fun shouldFetch(data: List<Game>?): Boolean =
                data == null || data.isEmpty()

        }.asFlow()
    }

    override fun getFavoriteGame(): Flow<List<Game>> {
        return localDataSource.getFavoriteGame().map {
            DataMapper.mapEntitiesToDomain(it)
        }
    }

    override suspend fun setFavoriteGame(game: Game, state: Boolean) {
        val gameEntity = DataMapper.mapDomainToEntity(game)
        localDataSource.setFavoriteGame(gameEntity, state)
    }

}