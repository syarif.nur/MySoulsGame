package com.threedotz.mysoulsgame.core.data.source.remote

import com.threedotz.mysoulsgame.core.data.source.remote.network.ApiResponse
import com.threedotz.mysoulsgame.core.data.source.remote.network.ApiService
import com.threedotz.mysoulsgame.core.data.source.remote.response.GameResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class RemoteDataSource(private val apiService: ApiService) {

    suspend fun getAllGame(): Flow<ApiResponse<List<GameResponse>>>{
        return flow{
            try {
                val response = apiService.getGame()
                val dataArray = response.result
                if (dataArray.isNotEmpty()){
                    emit(ApiResponse.Success(response.result))
                }else{
                    emit(ApiResponse.Empty)
                }
            }catch (e: Exception){
                emit(ApiResponse.Error(e.toString()))
            }
        }.flowOn(Dispatchers.IO)
    }
}