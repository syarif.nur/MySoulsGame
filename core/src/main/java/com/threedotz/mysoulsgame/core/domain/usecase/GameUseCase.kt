package com.threedotz.mysoulsgame.core.domain.usecase

import com.threedotz.mysoulsgame.core.data.Resource
import com.threedotz.mysoulsgame.core.domain.model.Game
import kotlinx.coroutines.flow.Flow

interface GameUseCase {
    fun getAllGame(): Flow<Resource<List<Game>>>
    fun getFavoriteGame(): Flow<List<Game>>
    suspend fun setFavoriteGame(game: Game, state: Boolean)
}