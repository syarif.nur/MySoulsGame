package com.threedotz.mysoulsgame.core.utils

import com.threedotz.mysoulsgame.core.data.source.local.entitiy.GameEntity
import com.threedotz.mysoulsgame.core.data.source.remote.response.GameResponse
import com.threedotz.mysoulsgame.core.domain.model.Game

object DataMapper {
    fun mapResponseToEntities(input: List<GameResponse>): List<GameEntity> {
        val gameList = ArrayList<GameEntity>()
        input.map {
            val game = GameEntity(
                it.id,
                it.name,
                it.released,
                it.imageUrl,
                it.rating,
                it.metacritic,
                false
            )
            gameList.add(game)
        }
        return gameList
    }

    fun mapEntitiesToDomain(input: List<GameEntity>): List<Game> =
        input.map {
            Game(
                it.id,
                it.name,
                it.released,
                it.imageUrl,
                it.rating,
                it.metacritic,
                isFavorite = it.isFavorite
            )
        }

    fun mapDomainToEntity(input: Game) = GameEntity(
        input.gameId,
        input.name,
        input.released,
        input.imageUrl,
        input.rating,
        input.metacritic,
        input.isFavorite
    )

}