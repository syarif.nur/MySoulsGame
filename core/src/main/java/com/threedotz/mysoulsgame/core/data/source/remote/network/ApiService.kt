package com.threedotz.mysoulsgame.core.data.source.remote.network

import com.threedotz.mysoulsgame.core.data.source.remote.response.ListGameResponse
import retrofit2.http.GET

interface ApiService {
    @GET("games?key=8c9d0fe30e98427287d6fd63afb1d552&creators=762,755,2984,26642&ordering=-released&exclude_additions=true&exclude_parents=false")
    suspend fun getGame(): ListGameResponse
}