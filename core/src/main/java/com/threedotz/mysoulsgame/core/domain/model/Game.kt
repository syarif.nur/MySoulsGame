package com.threedotz.mysoulsgame.core.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Game(
    val gameId: String,
    val name: String,
    val released: String,
    val imageUrl: String?,
    val rating: String?,
    val metacritic: String?,
    val isFavorite: Boolean
) : Parcelable