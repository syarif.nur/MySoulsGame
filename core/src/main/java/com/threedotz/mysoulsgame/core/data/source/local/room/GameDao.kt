package com.threedotz.mysoulsgame.core.data.source.local.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.threedotz.mysoulsgame.core.data.source.local.entitiy.GameEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface GameDao {

    @Query("SELECT * FROM game_list")
    fun getAllGame(): Flow<List<GameEntity>>

    @Query("SELECT * FROM game_list WHERE isFavorite = 1")
    fun getFavoriteGame(): Flow<List<GameEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertGame(game: List<GameEntity>)

    @Update
    suspend fun updateFavoriteGame(game: GameEntity)

}