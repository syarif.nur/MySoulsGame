package com.threedotz.mysoulsgame.favorite

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.threedotz.mysoulsgame.core.domain.usecase.GameUseCase

class FavoriteViewModel(gameUseCase: GameUseCase): ViewModel() {
    val favoriteGame = gameUseCase.getFavoriteGame().asLiveData()
}