package com.threedotz.mysoulsgame.favorite

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.threedotz.mysoulsgame.core.data.Resource
import com.threedotz.mysoulsgame.core.ui.GameAdapter
import com.threedotz.mysoulsgame.detail.DetailGameActivity
import com.threedotz.mysoulsgame.favorite.databinding.ActivityFavoriteBinding
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.context.loadKoinModules

class FavoriteActivity : AppCompatActivity() {

    private lateinit var binding: ActivityFavoriteBinding
    private val favoriteViewModel: FavoriteViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFavoriteBinding.inflate(layoutInflater)
        setContentView(binding.root)

        loadKoinModules(favoriteModule)

        supportActionBar?.title = "Favorite Game"

        val gameAdapter = GameAdapter()
        gameAdapter.onItemClick = {selectedData ->
            val intent = Intent(this, DetailGameActivity::class.java)
            intent.putExtra(DetailGameActivity.EXTRA_DATA,selectedData)
            startActivity(intent)
        }
        favoriteViewModel.favoriteGame.observe(this) { game ->
            gameAdapter.differ.submitList(game)
            binding.viewEmpty.root.visibility = if (game.isNotEmpty()) View.GONE else View.VISIBLE
        }
        with(binding.rvGame) {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = gameAdapter
        }
    }

}